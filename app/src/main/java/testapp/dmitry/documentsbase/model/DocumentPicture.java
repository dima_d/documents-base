package testapp.dmitry.documentsbase.model;

/**
 * Created by Dmitry Subbotenko on 28.02.2017.
 */

public class DocumentPicture {
    public Long _id; // for cupboard
    public byte[] picture;
}
