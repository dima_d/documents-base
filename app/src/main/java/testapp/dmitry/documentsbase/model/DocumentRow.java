package testapp.dmitry.documentsbase.model;

/**
 * Created by Dmitry Subbotenko on 28.02.2017.
 */

public class DocumentRow {
    public Long _id; // for cupboard
    public String name;
    public Long pictureId;
}
