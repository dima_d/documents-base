package testapp.dmitry.documentsbase.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

/**
 * Created by Dmitry Subbotenko on 28.02.2017.
 */

public class DataHelper extends SQLiteOpenHelper {

    public static final int VERSION = 1;
    public static final String NAME = "documents.db";

    static {
        cupboard().register(DocumentPicture.class);
        cupboard().register(DocumentRow.class);
    }

    public DataHelper(Context context) {
        super(context, NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        cupboard().withDatabase(db).createTables();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        cupboard().withDatabase(db).upgradeTables();
    }
}
