package testapp.dmitry.documentsbase.model;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import testapp.dmitry.documentsbase.utils.BoundService;
import testapp.dmitry.documentsbase.utils.ServiceConnector;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

/**
 * Created by Dmitry Subbotenko on 28.02.2017.
 */

public class ModelService extends BoundService {


    private DataHelper dataHelper;

    List<DocumentRow> documentRows = new ArrayList<>();


    public static class Connector extends ServiceConnector<ModelService> {
        public Connector(Context context) {
            super(context, ModelService.class);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        dataHelper = new DataHelper(this);

        SQLiteDatabase db = dataHelper.getReadableDatabase();
        documentRows.addAll(cupboard().withDatabase(db).query(DocumentRow.class).list());
        db.close();

        return super.onStartCommand(intent, flags, startId);
    }

    public List<DocumentRow> getDocuments() {
        return documentRows;
    }
    public byte[] getPicture(long id){
        SQLiteDatabase db = dataHelper.getReadableDatabase();
        DocumentPicture documentPicture = cupboard().withDatabase(db).get(DocumentPicture.class, id);
        db.close();
        return documentPicture.picture;
    }

    public void save(DocumentRow row) {
        if (!documentRows.contains(row))
            documentRows.add(row);

        SQLiteDatabase db = dataHelper.getWritableDatabase();
        cupboard().withDatabase(db).put(row);
        db.close();
    }

    public void remove(DocumentRow row) {
        SQLiteDatabase db = dataHelper.getWritableDatabase();
        documentRows.remove(row);

        if (row._id!=null){
            cupboard().withDatabase(db).delete(DocumentRow.class,row._id);
        }

        if(row.pictureId!=null){
            cupboard().withDatabase(db).delete(DocumentPicture.class,row.pictureId);
        }

        db.close();
    }

    public void savePicture(DocumentPicture picture, DocumentRow row){
        SQLiteDatabase db = dataHelper.getWritableDatabase();
        row.pictureId = cupboard().withDatabase(db).put(picture);
        db.close();
    }
}
