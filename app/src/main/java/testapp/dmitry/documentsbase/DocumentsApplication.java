package testapp.dmitry.documentsbase;

import android.app.Application;
import android.content.Intent;

import testapp.dmitry.documentsbase.model.ModelService;
import testapp.dmitry.documentsbase.presenter.PresenterService;

/**
 * Created by Dmitry Subbotenko on 28.02.2017.
 */

public class DocumentsApplication extends Application{
    @Override
    public void onCreate() {
        super.onCreate();

        startService(new Intent(getApplicationContext(),ModelService.class));
        startService(new Intent(getApplicationContext(),PresenterService.class));
    }
}
