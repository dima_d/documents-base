package testapp.dmitry.documentsbase.utils;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * Created by Dmitry Subbotenko on 28.02.2017.
 */
public class BoundService extends Service {
    private IBinder binder = new ServiceBinder(this);

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }
}
