package testapp.dmitry.documentsbase.utils;

import android.os.Binder;

/**
 * Created by Dmitry Subbotenko on 28.02.2017.
 */
public class ServiceBinder extends Binder {

    private BoundService service;

    public ServiceBinder(BoundService service) {
        this.service = service;
    }

    public BoundService getService() {
        return service;
    }
}
