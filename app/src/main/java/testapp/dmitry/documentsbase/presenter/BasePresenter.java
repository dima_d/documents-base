package testapp.dmitry.documentsbase.presenter;

import rx.Observable;
import rx.subjects.BehaviorSubject;
import testapp.dmitry.documentsbase.view.IMainView;

/**
 * Created by Dmitry Subbotenko on 01.03.2017.
 */

public abstract class BasePresenter<T> {
    private BehaviorSubject<T> viewSubject = BehaviorSubject.create();

    protected final Observable<IMainView> viewObservable;
    protected PresenterService presenterService;

    public BasePresenter(PresenterService presenterService) {

        this.presenterService = presenterService;
        viewObservable = presenterService.getMainView();
    }


    public void publishView(Observable<T> view){
        view.subscribe(
                v -> viewSubject.onNext(v),
                t->{},
                () -> viewSubject.onNext(null));
    }

    protected Observable<T> getView(){
        return viewSubject.filter(view -> view!=null).first();
    }

}
