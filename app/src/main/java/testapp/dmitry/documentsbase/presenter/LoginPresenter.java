package testapp.dmitry.documentsbase.presenter;

import android.content.SharedPreferences;

import rx.Observable;
import testapp.dmitry.documentsbase.R;
import testapp.dmitry.documentsbase.view.IMainView;

/**
 * Created by Dmitry Subbotenko on 28.02.2017.
 */

public class LoginPresenter {

    private final String PASSWORD_KEY = "password";
    private PresenterService presenterService;
    private final Observable<IMainView> viewObservable;
    private SharedPreferences preferences;


    public LoginPresenter(PresenterService presenterService) {

        this.presenterService = presenterService;
        viewObservable = presenterService.getMainView();
        preferences = presenterService.sharedPreferences;
    }

    public void showLoginScreen(){
        if(preferences.contains(PASSWORD_KEY)){
            viewObservable.subscribe(IMainView::login);
        } else {
            viewObservable.subscribe(IMainView::newLogin);
        }
   }
    public void signIn(CharSequence password ){
        if(preferences.contains(PASSWORD_KEY)){

            if ( preferences.getString(PASSWORD_KEY, "").equals(password.toString())){
                presenterService.getMainPresenter().showMainScreen();
            } else {
                viewObservable.subscribe(m -> m.showToast(R.string.incorrect_password));
            }
        } else {
            preferences.edit().putString(PASSWORD_KEY, password.toString()).apply();
            presenterService.getMainPresenter().showMainScreen();
        }
    }
}
