package testapp.dmitry.documentsbase.presenter;

import android.support.annotation.NonNull;
import android.support.v4.util.Pair;

import rx.Observable;
import testapp.dmitry.documentsbase.model.DocumentPicture;
import testapp.dmitry.documentsbase.model.DocumentRow;
import testapp.dmitry.documentsbase.view.IMainView;
import testapp.dmitry.documentsbase.view.IPreviewFragment;

/**
 * Created by Dmitry Subbotenko on 01.03.2017.
 */

public class PreviewPresenter extends BasePresenter<IPreviewFragment> {

    private DocumentRow row;

    public PreviewPresenter(PresenterService presenterService) {
        super(presenterService);
    }

    public void showPreviewScreen(@NonNull DocumentRow row){
        this.row = row;

        if (this.row == null)
            this.row = new DocumentRow();

        viewObservable.subscribe(IMainView::showPreviewScreen);
    }

    public void newPicture(byte[] data){
        row = new DocumentRow();
        DocumentPicture pic = new DocumentPicture();
        pic.picture = data;
        presenterService.getModelService().first().subscribe(modelService ->  modelService.savePicture(pic, row));

        viewObservable.subscribe(IMainView::showPreviewScreen);

    }

    public void save(CharSequence sequence) {
        row.name = sequence.toString();
        presenterService.getModelService().first().subscribe(modelService ->  modelService.save(row));
        presenterService.getMainPresenter().showMainScreen();
    }

    public void remove() {
        presenterService.getModelService().first().subscribe(modelService ->  modelService.remove(row));
        presenterService.getMainPresenter().showMainScreen();
    }

    public Observable<Pair<DocumentRow, byte[]>> getPreviewData() {
        return presenterService.getModelService().map(modelService -> new Pair<DocumentRow, byte[]>(row,modelService.getPicture(row.pictureId))).first();
    }
}
