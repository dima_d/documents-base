package testapp.dmitry.documentsbase.presenter;

import java.util.List;

import rx.Observable;
import rx.subjects.PublishSubject;
import testapp.dmitry.documentsbase.model.DocumentRow;
import testapp.dmitry.documentsbase.view.IMainFragment;
import testapp.dmitry.documentsbase.view.IMainView;

/**
 * Created by Dmitry Subbotenko on 28.02.2017.
 */

public class MainPresenter extends BasePresenter<IMainFragment> {


    public MainPresenter(PresenterService presenterService) {
        super(presenterService);
    }

    public void showMainScreen() {
        viewObservable.subscribe(IMainView::showMainScreen);
        Observable.zip(getView(), presenterService.getModelService(), (iMainFragment, modelService) -> {
            return null;
        }).first().subscribe();
    }


    public void publishOnClickSubject(Observable<DocumentRow> onClickSubject) {
        onClickSubject.subscribe(row -> {
            presenterService.getPreviewPresenter().showPreviewScreen(row);
        });
    }

    public void addItemClicked(){
        viewObservable.subscribe(IMainView::takeCameraShot);

    }

    public Observable<List<DocumentRow>> getData() {
        return presenterService.getModelService().map(m -> m.getDocuments()).first();
    }
}
