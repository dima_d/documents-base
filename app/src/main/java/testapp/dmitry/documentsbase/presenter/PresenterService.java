package testapp.dmitry.documentsbase.presenter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import rx.Observable;
import rx.subjects.BehaviorSubject;
import testapp.dmitry.documentsbase.model.ModelService;
import testapp.dmitry.documentsbase.utils.BoundService;
import testapp.dmitry.documentsbase.utils.ServiceConnector;
import testapp.dmitry.documentsbase.view.IMainView;

/**
 * Created by Dmitry Subbotenko on 28.02.2017.
 */

public class PresenterService extends BoundService {
    private static final String TAG = "PresenterService";

    protected MainPresenter mainPresenter;
    protected LoginPresenter loginPresenter;

    protected SharedPreferences sharedPreferences;
    private BehaviorSubject<IMainView> mainView = BehaviorSubject.create();
    private ModelService.Connector modelService;
    private PreviewPresenter previewPresenter;


    public static class Connector extends ServiceConnector<PresenterService> {
        public Connector(Context context, Observable.Transformer<PresenterService, PresenterService> lifeCycle) {
            super(context, PresenterService.class, lifeCycle);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        sharedPreferences = getSharedPreferences(TAG, MODE_PRIVATE);
        loginPresenter = new LoginPresenter(this);
        mainPresenter = new MainPresenter(this);
        previewPresenter = new PreviewPresenter(this);
        loginPresenter.showLoginScreen();

        modelService = new ModelService.Connector(this);

        return super.onStartCommand(intent, flags, startId);
    }

    public void initView(Observable<IMainView> iMainView){
        iMainView.subscribe(
                view -> mainView.onNext(view),
                t->{},
                () -> mainView.onNext(null));
    }

    public Observable<IMainView> getMainView(){
        return mainView.filter(mainView -> mainView!=null).first();
    }

    public LoginPresenter getLoginPresenter() {
        return loginPresenter;
    }

    public MainPresenter getMainPresenter() {
        return mainPresenter;
    }

    public PreviewPresenter getPreviewPresenter() {
        return previewPresenter;
    }

    public Observable<ModelService> getModelService() {
        return modelService.getService();
    }
}
