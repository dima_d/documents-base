package testapp.dmitry.documentsbase;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.subjects.BehaviorSubject;
import testapp.dmitry.documentsbase.presenter.PresenterService;
import testapp.dmitry.documentsbase.view.CameraActivity;
import testapp.dmitry.documentsbase.view.IMainView;
import testapp.dmitry.documentsbase.view.LoginFragment;
import testapp.dmitry.documentsbase.view.MainFragment;
import testapp.dmitry.documentsbase.view.NewLoginFragment;
import testapp.dmitry.documentsbase.view.PreviewFragment;

/**
 * Created by Dmitry Subbotenko on 28.02.2017.
 */
public class MainActivity extends BaseActivity implements IMainView {


    @BindView(R.id.content_view)
    FrameLayout contentView;
    public PresenterService.Connector presenterServiceConnector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

    }

    @Override
    protected void onResume() {
        presenterServiceConnector = new PresenterService.Connector(this, bindToLifecycle());
        presenterServiceConnector.getService().subscribe(s -> {
            s.initView(BehaviorSubject.create(this).compose(bindToLifecycle()));
        });

        super.onResume();
    }

    @Override
    public void login() {
        getSupportFragmentManager().beginTransaction().replace(R.id.content_view, new LoginFragment()).commit();
    }

    @Override
    public void newLogin() {
        getSupportFragmentManager().beginTransaction().replace(R.id.content_view, new NewLoginFragment()).commit();
    }

    @Override
    public void showMainScreen() {
        getSupportFragmentManager().beginTransaction().replace(R.id.content_view, new MainFragment()).commit();
    }

    @Override
    public void showPreviewScreen() {
        getSupportFragmentManager().beginTransaction().replace(R.id.content_view, new PreviewFragment()).commit();
    }


    private void moveToFront() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }


    @Override
    public void showToast(int resId) {
        Toast.makeText(this, resId, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void takeCameraShot() {
        Intent intent = new Intent(this, CameraActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        // TODO: Create navigation presenter
    }
}

