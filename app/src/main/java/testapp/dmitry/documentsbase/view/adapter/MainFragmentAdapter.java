package testapp.dmitry.documentsbase.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.subjects.PublishSubject;
import testapp.dmitry.documentsbase.R;
import testapp.dmitry.documentsbase.model.DocumentRow;

/**
 * Created by Dmitry Subbotenko on 28.02.2017.
 */

public class MainFragmentAdapter extends RecyclerView.Adapter<MainFragmentAdapter.MainFragmentViewHolder> {
    private List<DocumentRow> list = new ArrayList<>();
    PublishSubject<DocumentRow> onClickSubject = PublishSubject.create();


    @Override
    public MainFragmentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_main_item, parent, false);

        MainFragmentViewHolder vh = new MainFragmentViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MainFragmentViewHolder holder, int position) {
        holder.setUp(list.get(position));
     }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setData(List<DocumentRow> list) {
        this.list.clear();
        this.list.addAll(list);
        notifyDataSetChanged(); // TODO: add particularly notification
    }

    class MainFragmentViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.itemTextView)
        TextView itemTextView;
        private DocumentRow row;

        MainFragmentViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(v -> {
                onClickSubject.onNext(row);
            });
        }

        void setUp(DocumentRow row) {
            this.row = row;
            itemTextView.setText(row.name);
        }
    }

    public PublishSubject<DocumentRow> getOnClickSubject() {
        return onClickSubject;
    }
}
