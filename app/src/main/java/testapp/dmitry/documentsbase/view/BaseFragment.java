package testapp.dmitry.documentsbase.view;

import com.trello.rxlifecycle.components.support.RxFragment;

import rx.Observable;
import testapp.dmitry.documentsbase.MainActivity;
import testapp.dmitry.documentsbase.presenter.PresenterService;

/**
 * Created by Dmitry Subbotenko on 01.03.2017.
 */

public abstract class BaseFragment extends RxFragment {
    public MainActivity getTypedActivity(){
        return (MainActivity) getActivity();
    }

    public Observable<PresenterService> getPresenterService(){
        return ((MainActivity) getActivity()).presenterServiceConnector.getService();
    }
}
