package testapp.dmitry.documentsbase.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.subjects.BehaviorSubject;
import testapp.dmitry.documentsbase.R;
import testapp.dmitry.documentsbase.model.DocumentRow;
import testapp.dmitry.documentsbase.presenter.MainPresenter;
import testapp.dmitry.documentsbase.presenter.PresenterService;
import testapp.dmitry.documentsbase.view.adapter.MainFragmentAdapter;

/**
 * Created by Dmitry Subbotenko on 28.02.2017.
 */

public class MainFragment extends BaseFragment implements IMainFragment {

    private static final String TAG = "MainFragment";

    @BindView(R.id.content_view)
    RecyclerView contentView;
    private MainFragmentAdapter adapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);

        adapter = new MainFragmentAdapter();
        contentView.setLayoutManager(new LinearLayoutManager(getActivity()));
        contentView.setAdapter(adapter);

        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onResume() {
        getPresenterService()
                .map(PresenterService::getMainPresenter)
                .subscribe(p -> {
                    p.publishView(BehaviorSubject.create(this).compose(bindToLifecycle()));
                    p.publishOnClickSubject(adapter.getOnClickSubject().compose(bindToLifecycle()));
                    p.getData().subscribe(list -> adapter.setData(list));
                });



        super.onResume();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_add:
                getPresenterService()
                        .map(PresenterService::getMainPresenter).subscribe(MainPresenter::addItemClicked);

                break;
            default:
                Log.wtf(TAG,"Unknown menu item! " + item.getTitle());
                break;
        }

        return super.onOptionsItemSelected(item);
    }


}
