package testapp.dmitry.documentsbase.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.trello.rxlifecycle.components.support.RxFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import testapp.dmitry.documentsbase.MainActivity;
import testapp.dmitry.documentsbase.R;
import testapp.dmitry.documentsbase.presenter.PresenterService;

/**
 * Created by Dmitry Subbotenko on 28.02.2017.
 */

public class LoginFragment extends BaseFragment {
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.sign_in_button)
    Button signInButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.sign_in_button)
    public void onSignClick() {
        getPresenterService()
                .compose(bindToLifecycle())
                .subscribe(p -> p.getLoginPresenter().signIn(password.getText()));
    }
}
