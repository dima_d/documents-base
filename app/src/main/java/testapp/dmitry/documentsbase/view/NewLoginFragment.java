package testapp.dmitry.documentsbase.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.trello.rxlifecycle.components.support.RxFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import testapp.dmitry.documentsbase.R;
import testapp.dmitry.documentsbase.presenter.LoginPresenter;

/**
 * Created by Dmitry Subbotenko on 28.02.2017.
 */

public class NewLoginFragment extends LoginFragment {
    @Override
    public void onStart() {
        signInButton.setText(R.string.create_password);

        super.onStart();
    }
}
