package testapp.dmitry.documentsbase.view;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.subjects.BehaviorSubject;
import testapp.dmitry.documentsbase.R;
import testapp.dmitry.documentsbase.model.DocumentRow;
import testapp.dmitry.documentsbase.model.ModelService;
import testapp.dmitry.documentsbase.presenter.PresenterService;

/**
 * Created by Dmitry Subbotenko on 01.03.2017.
 */

public class PreviewFragment extends BaseFragment implements IPreviewFragment {
    @BindView(R.id.edit_doc_name)
    EditText editDocName;
    @BindView(R.id.image_preview)
    ImageView imagePreview;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_preview, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onResume() {
        getPresenterService()
                .map(PresenterService::getPreviewPresenter)
                .subscribe(p -> {
                    p.publishView(BehaviorSubject.create(this).compose(bindToLifecycle()));
                    p.getPreviewData().subscribe(data ->{
                        Bitmap bitmap = BitmapFactory.decodeByteArray(data.second, 0, data.second.length);
                        imagePreview.setImageBitmap(bitmap);
                        editDocName.setText(data.first.name);

                    });
                });

        super.onResume();
    }

    @OnClick(R.id.button_save)
    public void onSaveClick() {
        getPresenterService()
                .map(PresenterService::getPreviewPresenter).subscribe(p -> p.save(editDocName.getText()));
    }

    @OnClick(R.id.button_remove)
    public void onRemoveClick() {
        getPresenterService()
                .map(PresenterService::getPreviewPresenter).subscribe(p -> p.remove());
    }



}
