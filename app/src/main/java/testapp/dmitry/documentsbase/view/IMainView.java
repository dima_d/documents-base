package testapp.dmitry.documentsbase.view;

/**
 * Created by Dmitry Subbotenko on 28.02.2017.
 */

public interface IMainView {
    /**
     * Show password screen
     */
    void login();

    /**
     * Show create new  password screen
     */
    void newLogin();

    /**
     * Show main table screen.
     */
    void showMainScreen();

    /**
     * Show text in toast, length short
     */
    void showToast(int resId);

    /**
     * Run camera for taking shot.
     */
    void takeCameraShot();

    /**
     * Show preview fragment screen.
     */
    void showPreviewScreen();
}
